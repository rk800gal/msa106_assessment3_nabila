FROM python:3
RUN pip install flask
RUN pip install requests
RUN pip install flask_restful
EXPOSE 80
COPY books.py /
COPY templates /
COPY . .
CMD [ "python", "./books.py"]
