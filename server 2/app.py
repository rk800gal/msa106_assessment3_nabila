from flask import Flask, jsonify
from flask_restful import Resource, Api
import requests
import logging

app = Flask(__name__)
api = Api(app)

book_list = [
    {'id': 0,
     'title': 'Attack on Titan',
     'author': 'Hajime Isayama',
     'characters' : 'Eren Yeager, Mikasa Ackerman, Armin Arlert',
     'year_published': '2009'},
     
    {'id': 1,
     'title': 'Astrophysics for People in a Hurry',
     'author': 'Neil DeGrasse Tyson',
     'About': 'Basic questions about the universe',
     'published': '2017'},
]

book_list2 = requests.get('http://3.24.123.255/books').content
print(book_list2)

@app.route('/') #Route to Home Page
def home():
    return '''<h1> Reading Archive</h1>
<p>A what would u like to read today?.</p>'''

@app.route('/response', methods=['GET']) # route to get data on books
def getbook():
    return book_list2

if __name__ == '__main__':
    app.run('0.0.0.0','80')
    
