from flask import Flask, render_template
from flask_restful import Resource, Api
import requests

app = Flask(__name__, template_folder='templates')
api = Api(app)

# Create some test data for our catalog in the form of a list of dictionaries.

@app.route('/')
def Hello():
    return '''<h1> Health Check backend!</h1
    <p>Yes it's working :} </p>''' #just to make sure this works

@app.route('/books') #route to get data
def books():
    return render_template("index.json")
 
if __name__ == '__main__':
    app.run('0.0.0.0','80')
    
